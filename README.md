This project holds the NIST [Cybersecurity Framework](https://www.nist.gov/cyberframework/framework) requirements as [GitLab Requirements](https://gitlab.com/sam-s-test-group/nist-cybersecurity-framework-example/-/requirements_management/requirements)

These could be copied into a project that is being developed and then used to ensure that the various requirements are being met or not as part of an audit.
